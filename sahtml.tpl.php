<ul>
<li>Advisory ID: <?php print $node->field_advisory_id[0]['safe']; ?></li>
<li>Project: <?php print $node->field_project['link']; ?> (third-party module)</li>
<li>Version: <?php print $node->field_version['display'];?></li>
<li>Date: <?php print $node->field_date[0]['safe']; ?></li>
<li>Security risk: <?php print $node->field_risk[0]['safe']?> <a  href="http://drupal.org/security-team/risk-levels" >(definition of risk levels)</a></li>
<li>Exploitable from: <?php print $node->field_exploitable[0]['safe']?></li>
<li>Vulnerability: <?php print $node->field_vulnerability['display']?></li>
</ul>
<h2>Description</h2>
<?php print $node->field_description[0]['safe']; ?>
<h2>Versions affected</h2>
<?php print $node->field_versions_affected[0]['safe']; ?>
<p>Drupal core is not affected. If you do not use the contributed <?php print $node->field_project['link'];?> module, there is nothing you need to do.</p>
<h2>Solution</h2>
<?php print $node->field_solution[0]['safe']; ?>
<p>See also the <?php print $node->field_project['link']?> project page</a>.</p>
<h2>Reported by</h2>
<?php print $node->field_reported_by[0]['safe']; ?>
<h2>Fixed by</h2>
<?php print $node->field_fixed_by[0]['safe']; ?>
<h2>Contact and More Information</h2>
<p>The Drupal security team can be reached at security at drupal.org or via the contact form at <a href="http://drupal.org/contact" title="http://drupal.org/contact" >http://drupal.org/contact</a>.<br />
Learn more about <a href="http://drupal.org/security-team" >the team and their policies</a>, <a href="http://drupal.org/writing-secure-code" >writing secure code for Drupal</a>, and <a href="http://drupal.org/security/secure-configuration" >secure configuration</a> of your site.</p>
</div>
